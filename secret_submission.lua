local submissionwriter = require 'parsers.secretsubmissionwriter'

-----------------------------------------------------------------------------------------------
-- user input
-----------------------------------------------------------------------------------------------
local whichjson = {1,2,3,4,5,6,7,8,9}-- choose which json file you want to use from the list below


-----------------------------------------------------------------------------------------------
-- the list of input json files
-----------------------------------------------------------------------------------------------
local jsonfiles = {
  [1] = './outputjsons/01_dummy_results.json',
  [2] = './outputjsons/02_a_little_less_dummy_results.json',
  [3] = './outputjsons/03_FWA_0.125_results.json',
  [4] = './outputjsons/04_V1.02_FWA_without_obstruction_results.json',
  [5] = './outputjsons/05_V1.02_FWA_with_obstruction_results.json',
  [6] = './outputjsons/06_V1.20_FWA_results.json',
  [7] = './outputjsons/07_V1.22_FWA_results.json',
  [8] = './outputjsons/08_V1.30_FWA_results.json',
  [9] = './outputjsons/09_ZUE-ZG-CH_0600-1200_results.json'
}

local args = {}
for _, j in pairs(whichjson) do
  table.insert(args, jsonfiles[j])
end


-----------------------------------------------------------------------------------------------
-- prepare submission file
-----------------------------------------------------------------------------------------------
submissionwriter(args)