-----------------------------------------------------------------------------------------------
-- load the necessary packages
-----------------------------------------------------------------------------------------------
local lub   = require 'lub'
local lfs   = require 'lfs'
local json = require 'parsers.json'
local lustache = require "parsers.lustache"

-----------------------------------------------------------------------------------------------
-- create the lib class
-----------------------------------------------------------------------------------------------
local lib   = lub.class 'secretjson2ampldata'

function lib.new(args)
  -----------------------------------------------------------------------------------------------
  -- assign the function arguments to local params
  -----------------------------------------------------------------------------------------------
  local input_file = args.input_file
  local data_file = args.data_file

  -----------------------------------------------------------------------------------------------
  -- initiate the data table and the content of the data file
  -----------------------------------------------------------------------------------------------
  local data = {}
  local datacontent

  -----------------------------------------------------------------------------------------------
  -- load the mustache
  -----------------------------------------------------------------------------------------------
  local f = io.open('parsers/data.mustache',"r")
  local mustache = f:read("*all")
  f:close()

  -----------------------------------------------------------------------------------------------
  -- read the json content
  -----------------------------------------------------------------------------------------------
  f = io.open(input_file,"r")
  local jcontent
  if f then
    local text = f:read("*all")
    jcontent = json:decode(text)
  end
  f:close()

  -----------------------------------------------------------------------------------------------
  -- hash and label
  -----------------------------------------------------------------------------------------------
  data.hash = jcontent.hash
  data.label = jcontent.label

  -----------------------------------------------------------------------------------------------
  -- service intentions
  -----------------------------------------------------------------------------------------------
  data.service_intentions = jcontent.service_intentions
  data.connections = {}
  data.allserviceintentions = {}
  data.allroutes = {}
  data.allreqmarkers = {}
  data.interruptions = {}
  for _, tbl in pairs(data.service_intentions) do
    -----------------------------------------------------------------------------------------------
    -- change id with servid
    -----------------------------------------------------------------------------------------------
    tbl.servid = tbl.id
    if type(tbl.id) == 'string' then
      tbl.servid = tbl.servid:gsub('%[', 'ob')
      tbl.servid = tbl.servid:gsub('%]', 'cb')
    end
    tbl.id = nil

    if type(tbl.route) == 'string' then
      tbl.route = tbl.route:gsub('%[', 'ob')
      tbl.route = tbl.route:gsub('%]', 'cb')
    end

    table.insert(data.allserviceintentions, tbl.servid)
    local routenum = tonumber(tbl.servid) or tbl.servid
    data.allroutes[routenum] = tbl.servid
    
    if type(tbl.servid) == 'string' and tbl.servid:lower():match('int') then
      table.insert(data.interruptions, tbl.servid)
    end

    -----------------------------------------------------------------------------------------------
    -- convert string to seconds
    -----------------------------------------------------------------------------------------------
    local latestentry, latestexit, earliestentry, earliestexit = {}, {}, {}, {}
    for _, tbl2 in pairs(tbl.section_requirements) do
      data.allreqmarkers[tbl.servid] = data.allreqmarkers[tbl.servid] or {}
      data.allreqmarkers[tbl.servid][tbl2.section_marker] = true

      if tbl2.entry_earliest then
        local hour, minute, second = tbl2.entry_earliest:match('(%d+)%:(%d+)%:(%d+)')
        tbl2.entry_earliest = hour*3600 + minute*60 + second
        table.insert(earliestentry, {routeid = tbl.route, servid = tbl.servid, routesection = tbl2.sequence_number, marker = tbl2.section_marker})
      end

      if tbl2.exit_earliest then
        local hour, minute, second = tbl2.exit_earliest:match('(%d+)%:(%d+)%:(%d+)')
        tbl2.exit_earliest = hour*3600 + minute*60 + second
        table.insert(earliestexit, {routeid = tbl.route, servid = tbl.servid, routesection = tbl2.sequence_number, marker = tbl2.section_marker})
      end

      if tbl2.entry_latest then
        local hour, minute, second = tbl2.entry_latest:match('(%d+)%:(%d+)%:(%d+)')
        tbl2.entry_latest = hour*3600 + minute*60 + second
        table.insert(latestentry, {routeid = tbl.route, servid = tbl.servid, routesection = tbl2.sequence_number, marker = tbl2.section_marker})
      end

      if tbl2.exit_latest then
        local hour, minute, second = tbl2.exit_latest:match('(%d+)%:(%d+)%:(%d+)')
        tbl2.exit_latest = hour*3600 + minute*60 + second
        table.insert(latestexit, {routeid = tbl.route, servid = tbl.servid, routesection = tbl2.sequence_number, marker = tbl2.section_marker})
      end

      if tbl2.min_stopping_time then
        if tbl2.min_stopping_time:match('M') then
          local minmin, minsec = tbl2.min_stopping_time:match('PT(%d+)M(%d*)S*')
          minmin = tonumber(minmin)
          minsec = tonumber(minsec)
          tbl2.min_stopping_time = minmin*60 + (minsec and minsec or 0)
        else
          local minsec = tbl2.min_stopping_time:match('PT(%d+)S')
          tbl2.min_stopping_time = tonumber(minsec)
        end
      end

      if tbl2.connections then
        local connection = {}
        connection.servid = tbl.servid
        connection.routeid = tbl.route
        connection.sequence_number = tbl2.sequence_number
        connection.section_marker = tbl2.section_marker
        connection.connected = tbl2.connections
        table.insert(data.connections, connection)
        for _, connection in pairs(tbl2.connections) do
          if connection.min_connection_time then
            if connection.min_connection_time:match('M') then
              local minmin, minsec = connection.min_connection_time:match('PT(%d+)M(%d*)S*')
              minmin = tonumber(minmin)
              minsec = tonumber(minsec)
              connection.min_connection_time = minmin*60 + (minsec and minsec or 0)
            else
              local minsec = connection.min_connection_time:match('PT(%d+)S')
              connection.min_connection_time = tonumber(minsec)
            end
          end
        end
      end

      -----------------------------------------------------------------------------------------------
      -- change the type from string to number
      -----------------------------------------------------------------------------------------------
      if tbl2.type == 'start' then
        tbl2.type = 1 
      elseif tbl2.type == 'halt' then 
        tbl2.type = 2 
      elseif tbl2.type == 'ende' then
        tbl2.type = 3
      elseif tbl2.type == 'durchfahrt' then
        tbl2.type = 4
      end
    end
    if next(earliestentry) then
      data.service_intentions[_].earliestentry = earliestentry
    end
    if next(earliestexit) then
      data.service_intentions[_].earliestexit = earliestexit
    end
    if next(latestentry) then
      data.service_intentions[_].latestentry = latestentry
    end
    if next(latestexit) then
      data.service_intentions[_].latestexit = latestexit
    end
  end

  -----------------------------------------------------------------------------------------------
  -- routes
  -----------------------------------------------------------------------------------------------
  data.routes = jcontent.routes
  local allroutes = {}
  local allresources = {}
  local allsectionmarkers = {}
  local resourceofsection = {routes = {}}
  local allresourceofsection = {}
  local pathofroutesection = {}
  local markerofroutesection = {}
  for id1, tbl in pairs(data.routes) do
    -----------------------------------------------------------------------------------------------
    -- change id with routeid
    -----------------------------------------------------------------------------------------------
    tbl.routeid = tbl.id
    if type(tbl.routeid) == 'string' then
      tbl.routeid = tbl.routeid:gsub('%[', 'ob')
      tbl.routeid = tbl.routeid:gsub('%]', 'cb')
    end
    tbl.id = nil
    table.insert(allroutes, tbl.routeid)
    resourceofsection.routes[id1] = {routeid = tbl.routeid, route_paths = {}}
    allresourceofsection[tbl.routeid] = allresourceofsection[tbl.routeid] or {}
    pathofroutesection[tbl.routeid] = pathofroutesection[tbl.routeid] or {}
    markerofroutesection[tbl.routeid] = markerofroutesection[tbl.routeid] or {}
    for id2, tbl2 in pairs(tbl.route_paths) do
      tbl2.pathid = tbl2.id
      tbl2.id = nil
      resourceofsection.routes[id1].route_paths[id2] = {pathid = tbl2.pathid, route_sections = {}}
      allresourceofsection[tbl.routeid][tbl2.pathid] = allresourceofsection[tbl.routeid][tbl2.pathid] or {}
      for id3, tbl3 in pairs(tbl2.route_sections) do
        resourceofsection.routes[id1].route_paths[id2].route_sections[id3] = {sequence_number = tbl3.sequence_number, resource_occupations = {}}
        allresourceofsection[tbl.routeid][tbl2.pathid][tbl3.sequence_number] = allresourceofsection[tbl.routeid][tbl2.pathid][tbl3.sequence_number] or {}
        pathofroutesection[tbl.routeid][tbl3.sequence_number] = tbl2.pathid
        markerofroutesection[tbl.routeid][tbl3.sequence_number] = tbl3.section_marker and tbl3.section_marker[1]
        if tbl3.minimum_running_time:match('M') then
          local minmin, minsec = tbl3.minimum_running_time:match('PT(%d+)M(%d*)S*')
          minmin = tonumber(minmin)
          minsec = tonumber(minsec)
          tbl3.minimum_running_time = minmin*60 + (minsec and minsec or 0)
        else
          local minsec = tbl3.minimum_running_time:match('PT(%d+)S')
          tbl3.minimum_running_time = tonumber(minsec)
        end
        for _, tbl4 in pairs(tbl3.resource_occupations) do
          allresources[tbl4.resource] = true
          if not allresourceofsection[tbl.routeid][tbl2.pathid][tbl3.sequence_number][tbl4.resource] then
            allresourceofsection[tbl.routeid][tbl2.pathid][tbl3.sequence_number][tbl4.resource] = true
            table.insert(resourceofsection.routes[id1].route_paths[id2].route_sections[id3].resource_occupations, tbl4)
          end
        end
        if tbl3.section_marker and tbl3.section_marker[1] then
          allsectionmarkers[tbl3.section_marker[1]] = true
          tbl3.section_marker = tbl3.section_marker[1]
          if tbl3.section_marker == '' then
            tbl3.section_marker = nil
          end
        end
        tbl3.route_alternative_marker_at_entry = tbl3.route_alternative_marker_at_entry and tbl3.route_alternative_marker_at_entry[1]
        if tbl3.route_alternative_marker_at_entry == '' then
          tbl3.route_alternative_marker_at_entry = nil
        end
        tbl3.route_alternative_marker_at_exit = tbl3.route_alternative_marker_at_exit and tbl3.route_alternative_marker_at_exit[1]
        if tbl3.route_alternative_marker_at_exit == '' then
          tbl3.route_alternative_marker_at_exit = nil
        end
      end
    end
  end
  data.resourceofsection = resourceofsection
  data.pathofroutesection = pathofroutesection
  data.markerofroutesection = markerofroutesection

  -----------------------------------------------------------------------------------------------
  -- service intentions of routes
  -----------------------------------------------------------------------------------------------
  local siofroute = {}
  for _, route in pairs(allroutes) do
    local si = {}
    for _, tbl in pairs(data.service_intentions) do
      if tbl.route == route then
        table.insert(si, tbl.servid)
      end
    end
    table.insert(siofroute, {route = route, si = si})
  end
  data.siofroute = siofroute

  -----------------------------------------------------------------------------------------------
  -- resources
  -----------------------------------------------------------------------------------------------
  for _, tbl in pairs(jcontent.resources) do
    tbl.resourceid = tbl.id
    tbl.id = nil
    if tbl.release_time then
      if tbl.release_time:match('M') then
        local minmin, minsec = tbl.release_time:match('PT(%d+)M(%d*)S*')
        minmin = tonumber(minmin)
        minsec = tonumber(minsec)
        tbl.release_time = minmin*60 + (minsec and minsec or 0)
      else
        local minsec = tbl.release_time:match('PT(%d+)S')
        tbl.release_time = tonumber(minsec)
      end
    end
  end
  data.resources = jcontent.resources

  -----------------------------------------------------------------------------------------------
  -- section markers
  -----------------------------------------------------------------------------------------------
  data.sectionmarkers = {}
  for marker,_ in pairs(allsectionmarkers) do
    table.insert(data.sectionmarkers, marker)
  end

  -----------------------------------------------------------------------------------------------
  -- parse the lua table with the mustache
  -----------------------------------------------------------------------------------------------
  datacontent = lustache:render(mustache, data)

  -----------------------------------------------------------------------------------------------
  -- write the content to the ampl data file
  -----------------------------------------------------------------------------------------------
  f = io.open(data_file,"w")
  f:write(datacontent)
  f:close()

  return data
end

function lib.tablecompare(tbl1, tbl2)
  local comparison = {}
  for _, elm in pairs(tbl1) do
    comparison[_] = lib.ismember(tbl2, elm)
  end

  local status = true
  for idx, elm in pairs(comparison) do
    status = comparison[idx] == true and status or false
  end

  return status
end

function lib.ismember(tbl, elm)
  local temp = {}
  for _, idx in pairs(tbl) do
    temp[idx] = true
  end
  return temp[elm] or false
end

function lib.removeDuplicates(tbl)
  local hash, res = {}, {}
  for _,tbl2 in ipairs(tbl) do
    local v = ''
    for _, elm in pairs(tbl2.sections) do
      v = v..'_'..elm
    end

    if (not hash[v]) then
      res[#res+1] = tbl2
      hash[v] = true
    end
  end
  return res
end


return lib
