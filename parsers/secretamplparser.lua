-----------------------------------------------------------------------------------------------
-- load the necessary packages
-----------------------------------------------------------------------------------------------
local lub   = require 'lub'
local lfs   = require 'lfs'

-----------------------------------------------------------------------------------------------
-- create the lib class
-----------------------------------------------------------------------------------------------
local lib   = lub.class 'secretamplparser'

function lib.new(outputfile, pathofroutesection, markerofroutesection, si, allreqmarkers, problemnumber, routes)
  -----------------------------------------------------------------------------------------------
  -- check congergence
  -----------------------------------------------------------------------------------------------
  local convergence = true--lib:checkconvergence(problemnumber)

  if convergence then 
    -----------------------------------------------------------------------------------------------
    -- initiate tables
    -----------------------------------------------------------------------------------------------
    local results, entry_time, exit_time = {train_runs = {}}, {}, {}

    -----------------------------------------------------------------------------------------------
    -- read the ampl results
    -----------------------------------------------------------------------------------------------
    local f = io.open(outputfile,"r")

    -----------------------------------------------------------------------------------------------
    -- go through each line
    -----------------------------------------------------------------------------------------------
    for line in f:lines() do
      -----------------------------------------------------------------------------------------------
      -- parse the variable name, sets and the value
      -----------------------------------------------------------------------------------------------
      local var, setmembers, value  = line:match("(.*)%[(.*)%].*=%s*(.*)")

      -----------------------------------------------------------------------------------------------
      -- if the variable is entry_time
      -----------------------------------------------------------------------------------------------
      if var == 'entry_time' then
        if tonumber(value) ~= 0 then
          local route, rs = setmembers:match("(.*),'(.*)'")
          route = tonumber(route) or route
          if type(route)~= 'number' then
            route = route:gsub("'","")
          end
          rs = rs:gsub(route..'_', "")
          rs = tonumber(rs) or rs
          local siold = route

          local path = pathofroutesection[route][rs]
          local marker = markerofroutesection[route][rs] or 'nullResult'
          
          if type(route)~= 'number' then
            route = route:gsub("ob","[")
            route = route:gsub("cb","]")
          end
          
          local route_section_id = route .. '#' .. rs
          
          
--          route = routes[route]
          
          table.insert(entry_time, {route = route, path = path, routesection = rs, section_requirement = allreqmarkers[siold][marker] and marker or 'nullResult', route_section_id = route_section_id, si = route, entry_time = math.floor(tonumber(value)+0.5)})
        end
      end

      -----------------------------------------------------------------------------------------------
      -- if the variable is exit_time
      -----------------------------------------------------------------------------------------------
      if var == 'exit_time' then
        if tonumber(value) ~= 0 then
          local route, rs = setmembers:match("(.*),'(.*)'")
          route = tonumber(route) or route
          if type(route)~= 'number' then
            route = route:gsub("'","")
          end
          rs = rs:gsub(route..'_', "")
          rs = tonumber(rs) or rs
          local siold = route
          
          
          local path = pathofroutesection[route][rs]
          local marker = markerofroutesection[route][rs] or 'nullResult'
          
          if type(route)~= 'number' then
            route = route:gsub("ob","[")
            route = route:gsub("cb","]")
          end
          
          local route_section_id = route .. '#' .. rs
          
          
--          route = routes[route]
          
          table.insert(exit_time, {route = route, path = path, routesection = rs, section_requirement = allreqmarkers[siold][marker] and marker or 'nullResult', route_section_id = route_section_id, si = route, exit_time = math.floor(tonumber(value)+0.5)})
        end
      end
    end

    -----------------------------------------------------------------------------------------------
    -- add the exit time in entry time tables
    -----------------------------------------------------------------------------------------------
    for _, tbl in pairs(entry_time) do
      for _, tbl2 in pairs(exit_time) do
        if tbl.route_section_id == tbl2.route_section_id then
          tbl.exit_time = tbl2.exit_time
        end
      end
    end

    -----------------------------------------------------------------------------------------------
    -- structure the output in the desired format
    -----------------------------------------------------------------------------------------------
    for _, servid in pairs(si) do
      local servid2 = tonumber(servid) or servid
      if type(servid2)~= 'number' then
        servid2 = servid2:gsub("ob","[")
        servid2 = servid2:gsub("cb","]")
      end
      local train = {service_intention_id = servid2, train_run_sections = {}}
      -----------------------------------------------------------------------------------------------
      -- add the route sections
      -----------------------------------------------------------------------------------------------
      for _, tbl in pairs(entry_time) do
        if servid2 == tbl.si then
          table.insert(train.train_run_sections, {route = tbl.route, route_path = tbl.path, route_section_id = tbl.route_section_id, section_requirement = tbl.section_requirement, entry_time = tbl.entry_time, exit_time = tbl.exit_time})
        end
      end

      -----------------------------------------------------------------------------------------------
      -- sort the route sections in increasing order w.r.t. the entry time
      -----------------------------------------------------------------------------------------------
      table.sort(train.train_run_sections,  function(a,b) return a.entry_time < b.entry_time end)

      -----------------------------------------------------------------------------------------------
      -- add the sequence_number in the route sections and format the entry and exit times
      -----------------------------------------------------------------------------------------------
      for id, tbl in pairs(train.train_run_sections) do
        tbl.sequence_number = id
        local entryhours = math.floor(tbl.entry_time/3600)
        local entryminutes = math.floor((tbl.entry_time - entryhours * 3600) / 60)
        local entryseconds = tbl.entry_time - entryhours * 3600 - entryminutes * 60
        if string.format('%02i', entryseconds) == '60' then
          entryminutes = entryminutes + 1
          entryseconds = 0
        end
        tbl.entry_time = string.format('%02i:%02i:%02i', entryhours, entryminutes, entryseconds)

        local exithours = math.floor(tbl.exit_time/3600)
        local exitminutes = math.floor((tbl.exit_time - exithours * 3600) / 60)
        local exitseconds = tbl.exit_time - exithours * 3600 - exitminutes * 60
        if string.format('%02i', exitseconds) == '60' then
          exitminutes = exitminutes + 1
          exitseconds = 0
        end
        tbl.exit_time = string.format('%02i:%02i:%02i', exithours, exitminutes, exitseconds)
      end

      -----------------------------------------------------------------------------------------------
      -- add the train runs in the results
      -----------------------------------------------------------------------------------------------
      table.insert(results.train_runs, train)
    end

    return results
  else
    error('AMPL did not converge. Check the ampl output in the ampl folder!!!')
  end
end

function lib:checkconvergence(problemnumber)
  local solved = false
  local solve_result_num

  local solved_result_nums = {
    [0]   =   'optimal solution',
    [1]   =   'primal has unbounded optimal face',
    [2]   =   'optimal integer solution',
    [3]   =   'optimal integer solution within mipgap or absmipgap',
    [100] =   'best solution found, primal-dual feasible',
    [102] =   'optimal (non-)integer solution',
    [103] =   'optimal (non-)integer solution within mipgap or absmipgap',
    [110] =   'optimal with unscaled infeasibilities',
    [111] =   'integer optimal with unscaled infeasibilities',
    [200] =   'best solution found, primal infeasible',
    [201] =   'infeasible with phase II singularities',
    [202] =   'infeasible with phase I singularities',
    [204] =   'converged, dual feasible, primal infeasible',
    [205] =   'converged, primal and dual infeasible',
    [206] =   'best solution found, primal infeasible',
    [207] =   'best solution found, primal-dual infeasible',
    [208] =   'infeasible or unbounded in presolve',
    [209] =   'integer infeasible or unbounded in presolve',
    [210] =   'infeasible problem found by dualopt;.dunbdd returned',
    [220] =   'integer infeasible',
    [300] =   'unbounded problem',
    [301] =   'converged, primal feasible, dual infeasible',
    [302] =   'best solution found, dual infeasible',
    [310] =   'unbounded problem found by primalopt;.unbdd returned',
    [320] =   'integer unbounded ray',
    [400] =   'phase II objective limit exceeded',
    [401] =   'phase II iteration limit',
    [402] =   'phase I iteration limit',
    [403] =   'phase II time limit',
    [404] =   'phase I time limit',
    [405] =   'primal objective limit reached',
    [406] =   'dual objective limit reached',
    [410] =   'node limit with no integer solution',
    [411] =   'time limit with no integer solution',
    [412] =   'treememory limit with no integer solution',
    [413] =   'node file limit with no integer solution',
    [420] =   'mixed integer solutions limit',
    [421] =   'node limit with integer solution',
    [422] =   'time limit with integer solution',
    [423] =   'treememory limit with integer solution',
    [424] =   'node file limit with integer solution',
    [500] =   'unrecoverable failure',
    [501] =   'aborted in phase II',
    [502] =   'aborted in phase I',
    [503] =   'aborted in barrier, dual infeasible',
    [504] =   'aborted in barrier, primal infeasible',
    [505] =   'aborted in barrier, primal and dual infeasible',
    [506] =   'aborted in barrier, primal and dual feasible',
    [507] =   'aborted in crossover',
    [508] =   'solution found, numerical difficulties',
    [509] =   'solution found, inconsistent equations',
    [510] =   'unrecoverable failure with no integer solution',
    [511] =   'aborted, no integer solution',
    [512] =   'out of memory, no tree; no integer solution',
    [520] =   'unrecoverable failure with integer solution',
    [521] =   'aborted, integer solution exists',
    [523] =   'out of memory, no tree; solution may exist',
    [530] =   'bug? problem has no variables',
    [531] =   'bug? Error return from named CPLEX routine',
    [540] =   'Diagonal QP Hessian has elements of the wrong sign',
    [541] =   'QP Hessian has diag. elements of the wrong sign',
    [542] =   'QP Hessian is not positive definite',
    [550] =   'problem has (nonquadratic) nonlinear constraints',
    [551] =   'problem has a nonlinear objective',
    [552] =   'problem has nonlinear integer variables',
    [553] =   'problem has integer variables and a quadratic objective',
    [554] =   'problem has unlinearized piecewise-linear terms',
    [555] =   'problem has a quadratic objective involving division by 0',
    [556] =   'nonlinear objective without CPLEX Barrier option (for QPs)',
    [557] =   'CPLEX MIP option needed to handle piecewise-linear terms',
    [558] =   'quadratic constraint involves division by zero',
    [559] =   'bug: no quadratic terms in "nonlinear" constraint',
    [560] =   'error in $cplex_options',
    [561] =   'surprise return from a CPLEX routine (perhaps a driver bug)',
    [562] =   'constraint is not convex quadratic',
    [563] =   'logical constraint is not an indicator constraint',
    [570] =   'CPLEX licensing problem',
  }
  -----------------------------------------------------------------------------------------------
  local f = io.open('./ampl/outputfiles/problemsize_p'..problemnumber..'.out',"r")
  -----------------------------------------------------------------------------------------------
  if f then
    for line in f:lines() do
      local param, value  = line:match("([%w%-_]*)%s*=%s*([%w%-_]*)")
      if param == 'solve_result_num' then
        solve_result_num = tonumber(value)
        break
      end
    end
    -----------------------------------------------------------------------------------------------
    solved = solve_result_num < 200 and true
    print("'"..solve_result_num.."' ---> "..(solved_result_nums[solve_result_num] or 'solved?')..".\n")
    -----------------------------------------------------------------------------------------------
  else
    print("problemsize.out was not written. There should be an error while solving the model.")
  end

  return true
end

return lib