-----------------------------------------------------------------------------------------------
-- load the necessary packages
-----------------------------------------------------------------------------------------------
local lub   = require 'lub'
local lfs   = require 'lfs'
local lustache = require 'parsers.lustache'

-----------------------------------------------------------------------------------------------
-- create the lib class
-----------------------------------------------------------------------------------------------
local lib   = lub.class 'runfilewriter'

function lib.new(args, mustachename)
  local f = io.open(mustachename,"r")
  local mustache = f:read("*all")

  -----------------------------------------------------------------------------------------------
  -- parse the lua table with the mustache
  -----------------------------------------------------------------------------------------------
  local runcontent = lustache:render(mustache, args)

  -----------------------------------------------------------------------------------------------
  -- write the run file
  -----------------------------------------------------------------------------------------------
  f = io.open('ampl/secret.run', "w")
  f:write(runcontent)
  f:close()
end

return lib
