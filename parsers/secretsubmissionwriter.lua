-----------------------------------------------------------------------------------------------
-- load the necessary packages
-----------------------------------------------------------------------------------------------
local lub   = require 'lub'
local lfs   = require 'lfs'
local json = require 'parsers.json'

-----------------------------------------------------------------------------------------------
-- create the lib class
-----------------------------------------------------------------------------------------------
local lib   = lub.class 'secretamplparser'

function lib.new(problems)
  -----------------------------------------------------------------------------------------------
  -- initialise the submission table
  -----------------------------------------------------------------------------------------------
  local submission = {}
  
  for _, p in pairs(problems) do
    local f = io.open(p,"r")
    print(p)
    local text = f:read("*all")
    local jcontent = json:decode(text)
    table.insert(submission, jcontent)
  end
  
  local scontent = json:encode_pretty(submission, nil, { pretty = true, align_keys = false, indent = "", emptyArray = "nill", null = 'nullResult', removeSpace = true })
  local f = io.open('./outputjsons/secret_submission.json',"w")
  f:write(scontent)
  f:close()
end



return lib