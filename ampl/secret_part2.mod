
/*---------------------------------------------------------------------------------------------------------------------------------------
SBB challence
Fall 2018
Authors: 
	Francesco Baldi
	Hur Butun
	Ivan Kantor
	Luise Middelhauve
	Raluca Suciu
---------------------------------------------------------------------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------------------------------------------------------------------
Sets
---------------------------------------------------------------------------------------------------------------------------------------*/
set Routes default {} ordered;
set Interruptions default {};																																						#set of all routes
set RoutePaths{r in Routes}	default {};																																		#all the route paths of route r
set RouteSections{r in Routes, rp in RoutePaths[r]} default {}, ordered;																												#all the route sections of route path rp		
set SectionMarkers{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																				#the section marker of route section rs
set Markers default {};																																						#set of all markers
set RouteSectionsofMarkers{r in Routes, m in Markers} 																														#all the route sections with marker m
	:= setof {rp in RoutePaths[r], rs in RouteSections[r, rp], sm in SectionMarkers[r, rp, rs]: m = sm}(rs);
set RouteAlternativeMarkeratEntry{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																			#marker at entry for route section rs
set RouteAlternativeMarkeratExit{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																				#marker at exit for route section rs
set Resources default {};																																					#set of all resources
set ResourceOccupations{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {}, ordered;																			#the resources occupied on route section rs
set OntoServiceIntention{m in Markers, r in Routes} default {};	#service intention that service intention si has a connection to on a marked route section	
set OntoSectionMarker{m in Markers, r1 in Routes, r2 in OntoServiceIntention[m,r1]} default {};		#section marker that service intention si has a connection to on a marked route section	

set RouteSectionsOfRoute{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp]} (rs);															
/*---------------------------------------------------------------------------------------------------------------------------------------
Parameters
---------------------------------------------------------------------------------------------------------------------------------------*/																																								#problem instance hash
param SectionReqEntryEarliest{m in Markers, r in Routes};	
param SectionReqEntryLatest{m in Markers, r in Routes};	
param SectionReqExitEarliest{m in Markers, r in Routes};
param SectionReqExitLatest{m in Markers, r in Routes};	
param SectionReqEntryDelayWeight{m in Markers, r in Routes} default 0;
param SectionReqExitDelayWeight{m in Markers, r in Routes} default 0;
param minStoppingTime{m in Markers, r in Routes} default 0;
param Penalty{r in Routes, rs in RouteSectionsOfRoute[r]} default 0;																						#penalty for route section rs
param minRunningTime{r in Routes, rs in RouteSectionsOfRoute[r]} default 0;																				#minimum running time for route section rs
param minConnectionTime{m in Markers, r1 in Routes, r2 in OntoServiceIntention[m, r1]: r1 != r2} default 0;																	#minimum connection of a service intention si1 on a marked route section with a service intention si2				
param ReleaseTime {res in Resources}; #release time of resource res

param bigM := 200000;
param yRouteSections{r in Routes, rs in RouteSectionsOfRoute[r]} default 0;
/*---------------------------------------------------------------------------------------------------------------------------------------
Extra
---------------------------------------------------------------------------------------------------------------------------------------*/
set AlternativeMarkers{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], ram in RouteAlternativeMarkeratEntry[r, rp, rs]}(ram);
set RouteSectionswithMarkeratEntry{r in Routes, am in AlternativeMarkers[r]} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], m in RouteAlternativeMarkeratEntry[r, rp, rs]: m == am}(rs); #route sections with marker at entry m
set RouteSectionswithMarkeratExit{r in Routes, am in AlternativeMarkers[r]} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], m in RouteAlternativeMarkeratExit[r, rp, rs]: m == am}(rs); #route sections with marker at entry m
set FirstRouteSection{r in Routes, rp in RoutePaths[r]} := {rs in RouteSections[r, rp]: rs = first(RouteSections[r, rp])}; #setof{rs in RouteSections[r, rp]} first(rs);
set LastRouteSection{r in Routes, rp in RoutePaths[r]} := {rs in RouteSections[r, rp]: rs = last(RouteSections[r, rp])}; #setof{rs in RouteSections[r, rp]} last(rs); 
set AllRouteSections{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp]}(rs), ordered;
set AllRouteSectionswithMarkeratEntry{r in Routes} := setof{am in AlternativeMarkers[r], rs in RouteSectionswithMarkeratEntry[r, am]}(rs);
set AllRouteSectionswithMarkeratExit{r in Routes} := setof{am in AlternativeMarkers[r], rs in RouteSectionswithMarkeratExit[r, am]}(rs);
set StartRouteSections1{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], am in AlternativeMarkers[r]: rs in RouteSectionswithMarkeratEntry[r, am] and card(RouteSectionswithMarkeratExit[r, am]) == 0}(rs);
set StartRouteSections2{r in Routes} := setof{rp in RoutePaths[r], rs in FirstRouteSection[r, rp]: rs not in AllRouteSectionswithMarkeratEntry[r]}(rs);
set StartRouteSections{r in Routes} := StartRouteSections1[r] union StartRouteSections2[r];
set EndRouteSections1{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], am in AlternativeMarkers[r]: rs in RouteSectionswithMarkeratExit[r, am] and card(RouteSectionswithMarkeratEntry[r, am]) == 0}(rs);
set EndRouteSections2{r in Routes} := setof{rp in RoutePaths[r], rs in LastRouteSection[r, rp]: rs not in AllRouteSectionswithMarkeratExit[r]}(rs);
set EndRouteSections{r in Routes} := EndRouteSections1[r] union EndRouteSections2[r];
set NextRouteSections{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs not in AllRouteSectionswithMarkeratExit[r] and rs not in EndRouteSections[r]} := setof{rs2 in RouteSectionsOfRoute[r]: rs2==rs}next(rs);
set AllSectionswithNextRouteSections{r in Routes} := setof{rs in RouteSectionsOfRoute[r]: rs not in AllRouteSectionswithMarkeratExit[r] and rs not in EndRouteSections[r]}(rs);

set RouteSectionsWithResourceOccupations{r in Routes, rp in RoutePaths[r]} := setof{rs in RouteSections[r, rp], res in ResourceOccupations[r, rp, rs]} (rs);

set RouteSectionsWithResourceOccupations2 := setof{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp], res in ResourceOccupations[r, rp, rs]} (rs);


set MarkersWithEntryLatest{r in Routes};
set MarkersWithExitLatest{r in Routes};
set MarkersWithEntryEarliest{r in Routes};
set MarkersWithExitEarliest{r in Routes};


set RouteSectionsWithEntryLatest{r in Routes} := setof{m in MarkersWithEntryLatest[r], rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithExitLatest{r in Routes} := setof{m in MarkersWithExitLatest[r], rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithEntryEarliest{r in Routes} := setof{m in MarkersWithEntryEarliest[r], rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithExitEarliest{r in Routes} := setof{m in MarkersWithExitEarliest[r], rs in RouteSectionsofMarkers[r, m]} (rs);


set Sections := setof{r in Routes, rs in AllRouteSections[r]} (rs);
set RouteOfRouteSection{rs in Sections} := setof{r in Routes: rs in AllRouteSections[r]} (r);



set RouteSectionsofMarkers2{m in Markers} := setof{r in Routes, rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithMarkers := setof{m in Markers, rs in RouteSectionsofMarkers2[m]} (rs);
set MarkerOfRouteSection{rs in RouteSectionsWithMarkers} := setof{m in Markers: rs in RouteSectionsofMarkers2[m]} (m);

param RouteSectionEarliestEnrty{r in Routes, rs in RouteSectionsWithEntryEarliest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqEntryEarliest[m,r];

param RouteSectionEarliestExit{r in Routes, rs in RouteSectionsWithExitEarliest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqExitEarliest[m,r];

param RouteSectionLatestEnrty{r in Routes, rs in RouteSectionsWithEntryLatest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqEntryLatest[m,r];

param RouteSectionLatestExit{r in Routes, rs in RouteSectionsWithExitLatest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqExitLatest[m,r];

param StartRouteSectionsTime{r in Routes, rs in StartRouteSections[r]} :=
	if (exists{rs2 in RouteSectionsWithEntryEarliest[r]} rs2 = rs) then
		RouteSectionEarliestEnrty[r,rs]
		else if (exists{rs2 in RouteSectionsWithExitEarliest[r]} rs2 = rs) then
			RouteSectionEarliestExit[r,rs]
			else if (exists{rs2 in RouteSectionsWithEntryLatest[r]} rs2 = rs) then
				RouteSectionLatestEnrty[r,rs]
				else if (exists{rs2 in RouteSectionsWithExitLatest[r]} rs2 = rs) then
					RouteSectionLatestExit[r,rs];

param EndRouteSectionsTime{r in Routes, rs in EndRouteSections[r]} :=
	if (exists{rs2 in RouteSectionsWithEntryEarliest[r]} rs2 = rs) then
		RouteSectionEarliestEnrty[r,rs]
		else if (exists{rs2 in RouteSectionsWithExitEarliest[r]} rs2 = rs) then
			RouteSectionEarliestExit[r,rs]
			else if (exists{rs2 in RouteSectionsWithEntryLatest[r]} rs2 = rs) then
				RouteSectionLatestEnrty[r,rs]
				else if (exists{rs2 in RouteSectionsWithExitLatest[r]} rs2 = rs) then
					RouteSectionLatestExit[r,rs];


param StartRouteTime{r in Routes} := min{rs in StartRouteSections[r]} StartRouteSectionsTime[r, rs];
param EndRouteTime{r in Routes} := max{rs in EndRouteSections[r]} EndRouteSectionsTime[r, rs];

set RoutesOfSeparateTime{r in Routes} := setof{r2 in Routes: r2 != r and (EndRouteTime[r] + 301 < StartRouteTime[r2] or EndRouteTime[r2] + 301 < StartRouteTime[r])} (r2);
set RoutesOfSimilarTime{r in Routes} := setof{r2 in Routes: r2 != r and (r2 not in RoutesOfSeparateTime[r] or r2 in Interruptions or r in Interruptions)} (r2);

set ResourcesOfRoute{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], res in ResourceOccupations[r, rp, rs]} (res), ordered;

set SameRoutes := setof{r1 in Routes, r2 in Routes: (card(ResourcesOfRoute[r1]) == card(ResourcesOfRoute[r2])) 
	and (first(ResourcesOfRoute[r1]) == first(ResourcesOfRoute[r2])) and (last(ResourcesOfRoute[r1]) == last(ResourcesOfRoute[r2]))
	and r1 != r2
	and (card(AllRouteSections[r1]) == card(AllRouteSections[r2]))} (r1,r2);

set VerySimilarRoutes := setof{r1 in Routes, r2 in Routes: 
	(first(ResourcesOfRoute[r1]) == last(ResourcesOfRoute[r2]))
	and r1 != r2} (r1,r2);

set SameRoutes2{r1 in Routes} := setof{r2 in Routes: (card(ResourcesOfRoute[r1]) == card(ResourcesOfRoute[r2])) 
	and (first(ResourcesOfRoute[r1]) == first(ResourcesOfRoute[r2])) and (last(ResourcesOfRoute[r1]) == last(ResourcesOfRoute[r2]))
	and (card(AllRouteSections[r1]) == card(AllRouteSections[r2]))} (r2) ordered;

set SameRouteCouples{r1 in Routes, r2 in RoutesOfSimilarTime[r1]} := setof{r3 in SameRoutes2[r1], r4 in SameRoutes2[r2]: r4 in RoutesOfSimilarTime[r3] and r3 not in RoutesOfSimilarTime[r1]} (r3,r4);

set SameRouteCouples2{r1 in Routes, r2 in RoutesOfSimilarTime[r1]} := {(r1,r2)} union SameRouteCouples[r1,r2];

set MarkersWithConnection{r in Routes} := setof{m in Markers, o in OntoServiceIntention[m, r]} (m);
set RouteSectionsOfMarkers2{m in Markers} := setof{r in Routes, rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionswithConnection2{m in Markers} := setof{r in Routes, rs in RouteSectionsofMarkers[r, m]: m in MarkersWithConnection[r]} (rs);

set ConnectedRouteSections := setof{m1 in Markers, rs1 in RouteSectionswithConnection2[m1], r1 in RouteOfRouteSection[rs1], r2 in OntoServiceIntention[m1, r1], m2 in OntoSectionMarker[m1, r1, r2], rs2 in RouteSectionsOfMarkers2[m2]} (rs2);
set RouteSectionswithConnection3 := setof{m in Markers, r in Routes, rs in RouteSectionsofMarkers[r, m]: m in MarkersWithConnection[r]} (rs);
set RouteSectionswithConnection4 := ConnectedRouteSections union RouteSectionswithConnection3;
set RouteSectionsWithoutConnection := Sections diff RouteSectionswithConnection4;

set ConnectionConstraint := 
	setof{m1 in Markers, rs1 in RouteSectionswithConnection2[m1], r1 in RouteOfRouteSection[rs1], r2 in OntoServiceIntention[m1, r1], m2 in OntoSectionMarker[m1, r1, r2], rs2 in RouteSectionsOfMarkers2[m2]: 
		r2 in RouteOfRouteSection[rs2] and r1 != r2} (m1, rs1, r1, m2, rs2, r2);


set ResourceSharingSections{res in Resources} := setof{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp], res1 in ResourceOccupations[r, rp, rs]: res == res1 and yRouteSections[r,rs] == 1} (rs);

set ResourceSharingSectionsOfRoute{res in Resources, r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], res1 in ResourceOccupations[r, rp, rs]: res == res1 and yRouteSections[r,rs] == 1} (rs);

param entry_time_param{r in Routes, rs in AllRouteSections[r]};
param exit_time_param{r in Routes, rs in AllRouteSections[r]};

set InterruptionRouteSections := setof{r in Interruptions, rs in AllRouteSections[r]} (rs);

# set ResourcesAndRouteSections := 
# 	setof{res in Resources, rs1 in ResourceSharingSections[res], r1 in RouteOfRouteSection[rs1], rs2 in ResourceSharingSections[res], r2 in RouteOfRouteSection[rs2]:
# 	r1!=r2 and r2 in RoutesOfSimilarTime[r1] and (r1,r2) not in SameRoutes and (abs(entry_time_param[r1,rs1] - entry_time_param[r2,rs2]) <= 2500 or rs1 in InterruptionRouteSections or rs2 in InterruptionRouteSections)} (res,rs1,rs2); 

set ResourcesAndRouteSections := 
	setof{res in Resources, rs1 in ResourceSharingSections[res], r1 in RouteOfRouteSection[rs1], rs2 in ResourceSharingSections[res], r2 in RouteOfRouteSection[rs2]:
	r1!=r2 and r2 in RoutesOfSimilarTime[r1]
	and (abs(entry_time_param[r1,rs1]-entry_time_param[r2,rs2]) < 3100 or rs1 in InterruptionRouteSections or rs2 in InterruptionRouteSections)} (res,rs1,rs2); 


set NextRoutes{r1 in Routes} := setof{r2 in Routes: ord(r2) > ord(r1)} (r2);


set RouteSectionsSharingResource := setof{(res,rs1,rs2) in ResourcesAndRouteSections} (rs1,rs2); 

set RoutePathOfSection{rs in Sections} := setof{r in RouteOfRouteSection[rs], rp in RoutePaths[r], rs1 in RouteSections[r,rp]: rs == rs1} (rp);
set ResourceOccupations2{rs in Sections} := setof{r in RouteOfRouteSection[rs], rp in RoutePathOfSection[rs], res in ResourceOccupations[r,rp,rs]} (res);

set SharedResourceBetwenSections{(rs1,rs2) in RouteSectionsSharingResource} := setof{res1 in ResourceOccupations2[rs1], res2 in ResourceOccupations2[rs2]: res1 == res2} (res1);
param ReleaseTime2{(rs1,rs2) in RouteSectionsSharingResource} := max{res in SharedResourceBetwenSections[rs1,rs2]} ReleaseTime[res];

set RouteSectionsSharingResource2 := setof{(res,rs1,rs2) in ResourcesAndRouteSections, r1 in RouteOfRouteSection[rs1], r2 in RouteOfRouteSection[rs2]:
	r2 in NextRoutes[r1]} (rs1,rs2); 


set BeforeRouteSectionsnew{r1 in Routes, rs1 in AllRouteSections[r1]} := setof{rs2 in AllRouteSections[r1]: ord(rs1) - ord(rs2) <= 1000} (rs2);

set ConsecutiveRouteSectionsResource := setof{(res,rs1,rs2) in ResourcesAndRouteSections, r1 in RouteOfRouteSection[rs1], rs3 in BeforeRouteSectionsnew[r1,rs1]:
	rs3 in ResourceSharingSections[res]} (rs1,rs2,rs3);


# set ResourceDiffSections{(rs1,rs2) in RouteSectionsSharingResource} := (ResourceOccupations2[rs1] diff ResourceOccupations2[rs2]) union (ResourceOccupations2[rs2] diff ResourceOccupations2[rs1]);
# set SameSections := setof{(rs1,rs2) in RouteSectionsSharingResource: card(ResourceDiffSections[rs1,rs2]) == 0} (rs1,rs2);
set AllUsedRouteSections{r in Routes} := {rs in AllRouteSections[r]: yRouteSections[r,rs] == 1} ordered;
set FirstUsedRouteSection{r in Routes} := {first(AllUsedRouteSections[r])};
set LastUsedRouteSection{r in Routes} := {last(AllUsedRouteSections[r])};
set NextTrain{r in Routes} := setof{r1 in SameRoutes2[r], r2 in SameRoutes2[r]: r1 == r and ord(r2) == ord(r1) + 1} (r2);

set ResourcesAndRouteSections_test := 
	setof{(res,rs1,rs2) in ResourcesAndRouteSections, r1 in RouteOfRouteSection[rs1], r2 in RouteOfRouteSection[rs2]: r2 in NextRoutes[r1]} (res,rs1,rs2);


set RouteSectionsSharingResource_test := setof{(res,rs1,rs2) in ResourcesAndRouteSections_test} (rs1,rs2); 
/*---------------------------------------------------------------------------------------------------------------------------------------
Variables
---------------------------------------------------------------------------------------------------------------------------------------*/
var entry_time{r in Routes, rs in RouteSectionsOfRoute[r]: yRouteSections[r,rs] == 1} >= 0;
var exit_time{r in Routes, rs in RouteSectionsOfRoute[r]: yRouteSections[r,rs] == 1} >= 0;
var xResourcesSharing{(rs1, rs2) in RouteSectionsSharingResource} binary;

/*---------------------------------------------------------------------------------------------------------------------------------------
Constraints
---------------------------------------------------------------------------------------------------------------------------------------*/
subject to cSplitterMixerEntry1{r in Routes, am in AlternativeMarkers[r], rs1 in RouteSectionsOfRoute[r], rs2 in RouteSectionsOfRoute[r]
	:rs1 in RouteSectionswithMarkeratExit[r, am] and rs1 not in EndRouteSections[r] and rs2 in RouteSectionswithMarkeratEntry[r, am] and rs2 not in StartRouteSections[r] and yRouteSections[r,rs1] == 1 and yRouteSections[r,rs2] == 1}:
	entry_time[r, rs2] = exit_time[r, rs1];

subject to cNextRouteSectionEntry{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp], nrs in NextRouteSections[r, rp, rs]: rs in AllSectionswithNextRouteSections[r] and yRouteSections[r,rs] == 1}:
	entry_time[r, nrs] = exit_time[r, rs];

# subject to cNextTrain{r1 in Routes, rs1 in LastUsedRouteSection[r1], r2 in NextTrain[r1], rs2 in FirstUsedRouteSection[r2]}:
# 	entry_time[r2, rs2] >= exit_time[r1, rs1];

subject to c1Rule102{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithEntryEarliest[r] and yRouteSections[r,rs] == 1}:
	entry_time[r, rs] >= SectionReqEntryEarliest[m, r];

subject to c2Rule102{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithExitEarliest[r] and yRouteSections[r,rs] == 1}:
	exit_time[r, rs] >= SectionReqExitEarliest[m, r];

subject to c1Rule103_1{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and yRouteSections[r,rs] == 1}:
	exit_time[r, rs] - entry_time[r, rs] >= (minRunningTime[r, rs] + minStoppingTime[m, r]);

subject to c1Rule103_2{r in Routes, rs in RouteSectionsOfRoute[r]: yRouteSections[r,rs] == 1}:
	exit_time[r, rs] - entry_time[r, rs] >= minRunningTime[r, rs];

subject to c1Rule104_1{(rs1, rs2) in RouteSectionsSharingResource, r1 in RouteOfRouteSection[rs1], r2 in RouteOfRouteSection[rs2]: yRouteSections[r1,rs1] == 1 and yRouteSections[r2,rs2] == 1}:
	entry_time[r1, rs1] + bigM * (xResourcesSharing[rs1, rs2]) >= exit_time[r2, rs2] + ReleaseTime2[rs1,rs2];

subject to c1Rule104_1_b1{(rs1,rs2,rs3) in ConsecutiveRouteSectionsResource: (rs3,rs2) in RouteSectionsSharingResource}:
	xResourcesSharing[rs1, rs2] = xResourcesSharing[rs3, rs2];

# subject to c1Rule104_1_test{(rs1,rs2,rs3,rs4) in SameRouteSectionCouples}:
# 	xResourcesSharing[rs1, rs2] = xResourcesSharing[rs3, rs4];

subject to c1Rule104_2{(rs1, rs2) in RouteSectionsSharingResource_test}:
 	xResourcesSharing[rs1, rs2] + xResourcesSharing[rs2, rs1] = 1;

subject to c1Rule105{(m1, rs1, r1, m2, rs2, r2) in ConnectionConstraint: yRouteSections[r1,rs1] == 1 and yRouteSections[r2,rs2] == 1}:
	exit_time[r2, rs2] >= entry_time[r1, rs1] + minConnectionTime[m1, r1, r2];


var dummyEntry{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]: rs in RouteSectionsofMarkers[r, m] and yRouteSections[r,rs] == 1} >= 0;
var dummyExit{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]: rs in RouteSectionsofMarkers[r, m] and yRouteSections[r,rs] == 1} >= 0;
subject to cdummy1{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithEntryLatest[r] and yRouteSections[r,rs] == 1}:
	dummyEntry[m, r, rs] >= entry_time[r, rs] - SectionReqEntryLatest[m, r];
subject to cdummy3{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithExitLatest[r] and yRouteSections[r,rs] == 1}:
	dummyExit[m, r, rs] >= exit_time[r, rs] - SectionReqExitLatest[m, r];


var EntryDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} >= 0;
subject to cEntryDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m] and yRouteSections[r,rs] == 1}:
	EntryDelayPenaltyOfRouteSection[m, r, rp, rs] = SectionReqEntryDelayWeight[m, r] * dummyEntry[m, r, rs];

var EntryDelayPenaltyOfRoutePath{m in Markers, r in Routes, rp in RoutePaths[r]} =
	sum{rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} EntryDelayPenaltyOfRouteSection[m, r, rp, rs];

var EntryDelayPenaltyOfRoute{m in Markers, r in Routes} =
	sum{rp in RoutePaths[r]} EntryDelayPenaltyOfRoutePath[m, r, rp];

var ExitDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} >= 0;
subject to cExitDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m] and yRouteSections[r,rs] == 1}:
	ExitDelayPenaltyOfRouteSection[m, r, rp, rs] = SectionReqExitDelayWeight[m, r] * dummyExit[m, r, rs];

var ExitDelayPenaltyOfRoutePath{m in Markers, r in Routes, rp in RoutePaths[r]} =
	sum{rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} ExitDelayPenaltyOfRouteSection[m, r, rp, rs];

var ExitDelayPenaltyOfRoute{m in Markers, r in Routes} =
	sum{rp in RoutePaths[r]} ExitDelayPenaltyOfRoutePath[m, r, rp];

var DelayPenalty = (1/60) * sum{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]}
	(EntryDelayPenaltyOfRouteSection[m, r, rp, rs] + ExitDelayPenaltyOfRouteSection[m, r, rp, rs]);
var PathPenaltyOfRouteSection{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;
subject to cPathPenaltyOfRouteSection{r in Routes, rs in RouteSectionsOfRoute[r]}:
	PathPenaltyOfRouteSection[r, rs] = Penalty[r, rs]*yRouteSections[r, rs];
var PathPenalty >= 0;
subject to cPathPenalty:
	PathPenalty = sum{r in Routes, rs in RouteSectionsOfRoute[r]}
	PathPenaltyOfRouteSection[r, rs];

var obj_fucntion = DelayPenalty + PathPenalty;

/*---------------------------------------------------------------------------------------------------------------------------------------
Objective function
---------------------------------------------------------------------------------------------------------------------------------------*/
minimize objective: obj_fucntion;


