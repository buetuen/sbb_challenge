
/*---------------------------------------------------------------------------------------------------------------------------------------
SBB challence
Fall 2018
Authors: 
	Francesco Baldi
	Hur Butun
	Ivan Kantor
	Luise Middelhauve
	Raluca Suciu
---------------------------------------------------------------------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------------------------------------------------------------------
Sets
---------------------------------------------------------------------------------------------------------------------------------------*/
set Routes default {} ordered;	
set Interruptions default {};																																			#set of all routes
set RoutePaths{r in Routes}	default {};																																		#all the route paths of route r
set RouteSections{r in Routes, rp in RoutePaths[r]} default {}, ordered;																												#all the route sections of route path rp		
set SectionMarkers{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																				#the section marker of route section rs
set Markers default {};																																						#set of all markers
set RouteSectionsofMarkers{r in Routes, m in Markers} 																														#all the route sections with marker m
	:= setof {rp in RoutePaths[r], rs in RouteSections[r, rp], sm in SectionMarkers[r, rp, rs]: m = sm}(rs);
set RouteAlternativeMarkeratEntry{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																			#marker at entry for route section rs
set RouteAlternativeMarkeratExit{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																				#marker at exit for route section rs
set Resources default {};																																					#set of all resources
set ResourceOccupations{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]} default {};																			#the resources occupied on route section rs
set OntoServiceIntention{m in Markers, r in Routes} default {};	#service intention that service intention si has a connection to on a marked route section	
set OntoSectionMarker{m in Markers, r1 in Routes, r2 in OntoServiceIntention[m,r1]} default {};		#section marker that service intention si has a connection to on a marked route section	

set RouteSectionsOfRoute{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp]} (rs);

/*---------------------------------------------------------------------------------------------------------------------------------------
Parameters
---------------------------------------------------------------------------------------------------------------------------------------*/
param SectionReqEntryEarliest{m in Markers, r in Routes};	
param SectionReqEntryLatest{m in Markers, r in Routes};	
param SectionReqExitEarliest{m in Markers, r in Routes};
param SectionReqExitLatest{m in Markers, r in Routes};	
param SectionReqEntryDelayWeight{m in Markers, r in Routes} default 0;
param SectionReqExitDelayWeight{m in Markers, r in Routes} default 0;
param minStoppingTime{m in Markers, r in Routes} default 0;
param Penalty{r in Routes, rs in RouteSectionsOfRoute[r]} default 0;																						#penalty for route section rs
param minRunningTime{r in Routes, rs in RouteSectionsOfRoute[r]} default 0;																				#minimum running time for route section rs
param minConnectionTime{m in Markers, r1 in Routes, r2 in OntoServiceIntention[m, r1]: r1 != r2} default 0;																	#minimum connection of a service intention si1 on a marked route section with a service intention si2				
param ReleaseTime {res in Resources}; #release time of resource res

param bigM := 200000;

/*---------------------------------------------------------------------------------------------------------------------------------------
Extra
---------------------------------------------------------------------------------------------------------------------------------------*/
set AlternativeMarkers{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], ram in RouteAlternativeMarkeratEntry[r, rp, rs]}(ram);
set RouteSectionswithMarkeratEntry{r in Routes, am in AlternativeMarkers[r]} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], m in RouteAlternativeMarkeratEntry[r, rp, rs]: m == am}(rs); #route sections with marker at entry m
set RouteSectionswithMarkeratExit{r in Routes, am in AlternativeMarkers[r]} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], m in RouteAlternativeMarkeratExit[r, rp, rs]: m == am}(rs); #route sections with marker at entry m
set FirstRouteSection{r in Routes, rp in RoutePaths[r]} := {rs in RouteSections[r, rp]: rs = first(RouteSections[r, rp])}; #setof{rs in RouteSections[r, rp]} first(rs);
set LastRouteSection{r in Routes, rp in RoutePaths[r]} := {rs in RouteSections[r, rp]: rs = last(RouteSections[r, rp])}; #setof{rs in RouteSections[r, rp]} last(rs); 
set AllRouteSections{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp]}(rs), ordered;
set AllRouteSectionswithMarkeratEntry{r in Routes} := setof{am in AlternativeMarkers[r], rs in RouteSectionswithMarkeratEntry[r, am]}(rs);
set AllRouteSectionswithMarkeratExit{r in Routes} := setof{am in AlternativeMarkers[r], rs in RouteSectionswithMarkeratExit[r, am]}(rs);
set StartRouteSections1{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], am in AlternativeMarkers[r]: rs in RouteSectionswithMarkeratEntry[r, am] and card(RouteSectionswithMarkeratExit[r, am]) == 0}(rs);
set StartRouteSections2{r in Routes} := setof{rp in RoutePaths[r], rs in FirstRouteSection[r, rp]: rs not in AllRouteSectionswithMarkeratEntry[r]}(rs);
set StartRouteSections{r in Routes} := StartRouteSections1[r] union StartRouteSections2[r];
set EndRouteSections1{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], am in AlternativeMarkers[r]: rs in RouteSectionswithMarkeratExit[r, am] and card(RouteSectionswithMarkeratEntry[r, am]) == 0}(rs);
set EndRouteSections2{r in Routes} := setof{rp in RoutePaths[r], rs in LastRouteSection[r, rp]: rs not in AllRouteSectionswithMarkeratExit[r]}(rs);
set EndRouteSections{r in Routes} := EndRouteSections1[r] union EndRouteSections2[r];
set NextRouteSections{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs not in AllRouteSectionswithMarkeratExit[r] and rs not in EndRouteSections[r]} := setof{rs2 in RouteSectionsOfRoute[r]: rs2==rs}next(rs);
set AllSectionswithNextRouteSections{r in Routes} := setof{rs in RouteSectionsOfRoute[r]: rs not in AllRouteSectionswithMarkeratExit[r] and rs not in EndRouteSections[r]}(rs);

set RouteSectionsWithResourceOccupations{r in Routes, rp in RoutePaths[r]} := setof{rs in RouteSections[r, rp], res in ResourceOccupations[r, rp, rs]} (rs);


set MarkersWithEntryLatest{r in Routes};
set MarkersWithExitLatest{r in Routes};
set MarkersWithEntryEarliest{r in Routes};
set MarkersWithExitEarliest{r in Routes};


set RouteSectionsWithEntryLatest{r in Routes} := setof{m in MarkersWithEntryLatest[r], rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithExitLatest{r in Routes} := setof{m in MarkersWithExitLatest[r], rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithEntryEarliest{r in Routes} := setof{m in MarkersWithEntryEarliest[r], rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithExitEarliest{r in Routes} := setof{m in MarkersWithExitEarliest[r], rs in RouteSectionsofMarkers[r, m]} (rs);


set Sections := setof{r in Routes, rs in AllRouteSections[r]} (rs);
set RouteOfRouteSection{rs in Sections} := setof{r in Routes: rs in AllRouteSections[r]} (r);



set RouteSectionsofMarkers2{m in Markers} := setof{r in Routes, rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionsWithMarkers := setof{m in Markers, rs in RouteSectionsofMarkers2[m]} (rs);
set MarkerOfRouteSection{rs in RouteSectionsWithMarkers} := setof{m in Markers: rs in RouteSectionsofMarkers2[m]} (m);

param RouteSectionEarliestEnrty{r in Routes, rs in RouteSectionsWithEntryEarliest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqEntryEarliest[m,r];

param RouteSectionEarliestExit{r in Routes, rs in RouteSectionsWithExitEarliest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqExitEarliest[m,r];

param RouteSectionLatestEnrty{r in Routes, rs in RouteSectionsWithEntryLatest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqEntryLatest[m,r];

param RouteSectionLatestExit{r in Routes, rs in RouteSectionsWithExitLatest[r]} :=
	sum{rs2 in Sections,m in MarkerOfRouteSection[rs]: rs2 == rs} SectionReqExitLatest[m,r];

param StartRouteSectionsTime{r in Routes, rs in StartRouteSections[r]} :=
	if (exists{rs2 in RouteSectionsWithEntryEarliest[r]} rs2 = rs) then
		RouteSectionEarliestEnrty[r,rs]
		else if (exists{rs2 in RouteSectionsWithExitEarliest[r]} rs2 = rs) then
			RouteSectionEarliestExit[r,rs]
			else if (exists{rs2 in RouteSectionsWithEntryLatest[r]} rs2 = rs) then
				RouteSectionLatestEnrty[r,rs]
				else if (exists{rs2 in RouteSectionsWithExitLatest[r]} rs2 = rs) then
					RouteSectionLatestExit[r,rs];

param EndRouteSectionsTime{r in Routes, rs in EndRouteSections[r]} :=
	if (exists{rs2 in RouteSectionsWithEntryEarliest[r]} rs2 = rs) then
		RouteSectionEarliestEnrty[r,rs]
		else if (exists{rs2 in RouteSectionsWithExitEarliest[r]} rs2 = rs) then
			RouteSectionEarliestExit[r,rs]
			else if (exists{rs2 in RouteSectionsWithEntryLatest[r]} rs2 = rs) then
				RouteSectionLatestEnrty[r,rs]
				else if (exists{rs2 in RouteSectionsWithExitLatest[r]} rs2 = rs) then
					RouteSectionLatestExit[r,rs];


param StartRouteTime{r in Routes} := min{rs in StartRouteSections[r]} StartRouteSectionsTime[r, rs];
param EndRouteTime{r in Routes} := max{rs in EndRouteSections[r]} EndRouteSectionsTime[r, rs];

set RoutesOfSeparateTime{r in Routes} := setof{r2 in Routes: r2 != r and (EndRouteTime[r] + 301 < StartRouteTime[r2] or EndRouteTime[r2] + 301 < StartRouteTime[r])} (r2);
set RoutesOfSimilarTime{r in Routes} := setof{r2 in Routes: r2 != r and (r2 not in RoutesOfSeparateTime[r] or r2 in Interruptions or r in Interruptions)} (r2);

set RoutesOfTooSimilarTime{r in Routes} := setof{r2 in Routes: r2 != r and abs(StartRouteTime[r] - StartRouteTime[r2]) < 600 and abs(EndRouteTime[r] - EndRouteTime[r2]) < 600} (r2);


set ResourcesOfRoute{r in Routes} := setof{rp in RoutePaths[r], rs in RouteSections[r, rp], res in ResourceOccupations[r, rp, rs]} (res), ordered;

set SameRoutes := setof{r1 in Routes, r2 in Routes: (card(ResourcesOfRoute[r1]) == card(ResourcesOfRoute[r2])) 
	and (first(ResourcesOfRoute[r1]) == first(ResourcesOfRoute[r2])) and (last(ResourcesOfRoute[r1]) == last(ResourcesOfRoute[r2])) and r1 != r2
	and (card(AllRouteSections[r1]) == card(AllRouteSections[r2]))} (r1,r2);

set SameRoutes2{r1 in Routes} := setof{r2 in Routes: (card(ResourcesOfRoute[r1]) == card(ResourcesOfRoute[r2])) 
	and (first(ResourcesOfRoute[r1]) == first(ResourcesOfRoute[r2])) and (last(ResourcesOfRoute[r1]) == last(ResourcesOfRoute[r2]))} (r2);


set SharedResourcesOfRoutes{r1 in Routes, r2 in RoutesOfTooSimilarTime[r1]} := setof{res1 in ResourcesOfRoute[r1], res2 in ResourcesOfRoute[r2]: res1 == res2} (res1) ordered;

set FirstSharedResource{r1 in Routes, r2 in RoutesOfTooSimilarTime[r1]} := {res in SharedResourcesOfRoutes[r1,r2]: res = first(SharedResourcesOfRoutes[r1,r2])};

set LocationOfSharedResourceInRoutes{r1 in Routes, r2 in RoutesOfTooSimilarTime[r1]} := setof{res1 in ResourcesOfRoute[r1], res2 in ResourcesOfRoute[r2]: res1 == res2 and res1 in FirstSharedResource[r1,r2]} (ord(res1), ord(res2));

set PriorityTrains{r1 in Routes} := setof{r2 in RoutesOfTooSimilarTime[r1], (loc1, loc2) in LocationOfSharedResourceInRoutes[r1,r2]: loc1 - loc2 > 5} (r2);

set MarkersWithConnection{r in Routes} := setof{m in Markers, o in OntoServiceIntention[m, r]} (m);
set RouteSectionsOfMarkers2{m in Markers} := setof{r in Routes, rs in RouteSectionsofMarkers[r, m]} (rs);
set RouteSectionswithConnection2{m in Markers} := setof{r in Routes, rs in RouteSectionsofMarkers[r, m]: m in MarkersWithConnection[r]} (rs);

set ConnectedRouteSections := setof{m1 in Markers, rs1 in RouteSectionswithConnection2[m1], r1 in RouteOfRouteSection[rs1], r2 in OntoServiceIntention[m1, r1], m2 in OntoSectionMarker[m1, r1, r2], rs2 in RouteSectionsOfMarkers2[m2]} (rs2);
set RouteSectionswithConnection3 := setof{m in Markers, r in Routes, rs in RouteSectionsofMarkers[r, m]: m in MarkersWithConnection[r]} (rs);
set RouteSectionswithConnection4 := ConnectedRouteSections union RouteSectionswithConnection3;
set RouteSectionsWithoutConnection := Sections diff RouteSectionswithConnection4;

set ConnectionConstraint := 
	setof{m1 in Markers, rs1 in RouteSectionswithConnection2[m1], r1 in RouteOfRouteSection[rs1], r2 in OntoServiceIntention[m1, r1], m2 in OntoSectionMarker[m1, r1, r2], rs2 in RouteSectionsOfMarkers2[m2]: 
		r2 in RouteOfRouteSection[rs2] and r1 != r2} (m1, rs1, r1, m2, rs2, r2);


set ResourceSharingSections{res in Resources} := setof{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp], res1 in ResourceOccupations[r, rp, rs]: res == res1} (rs);

set PrecedingRoutes{r1 in Routes} := setof{r2 in Routes: ord(r2) < ord(r1)} (r2);
set NextRoutes{r1 in Routes} := setof{r2 in Routes: ord(r2) > ord(r1)} (r2);

set ResourcesAndRouteSections := 
	setof{res in Resources, rs1 in ResourceSharingSections[res], r1 in RouteOfRouteSection[rs1], rs2 in ResourceSharingSections[res], r2 in RouteOfRouteSection[rs2]:
	r1!=r2 and r2 in RoutesOfSimilarTime[r1] and r2 in NextRoutes[r1] and (r1,r2) not in SameRoutes} (res,rs1,rs2); 


set RouteSectionsSharingResource := setof{(res,rs1,rs2) in ResourcesAndRouteSections} (rs1,rs2); 
set SharedResource{(rs1,rs2) in RouteSectionsSharingResource} := setof{(res,rs3,rs4) in ResourcesAndRouteSections: rs1 == rs3 and rs2 == rs4} (res);

set PreviousSameRoutes{r1 in Routes} := setof{r2 in Routes: r2 in SameRoutes2[r1] and ord(r2) <= ord(r1)} (r2);
set OriginRoute{r1 in Routes} := setof{r2 in PreviousSameRoutes[r1]: card(PreviousSameRoutes[r2]) == 1} (r2);

set SameRouteCouples{r1 in Routes, r2 in RoutesOfSimilarTime[r1]} := setof{r3 in SameRoutes2[r1], r4 in RoutesOfSimilarTime[r3]: r3 not in RoutesOfSimilarTime[r1] and r4 in SameRoutes2[r2]
	and card(RoutesOfSimilarTime[r1]) == card(RoutesOfSimilarTime[r3]) and card(RoutesOfSimilarTime[r2]) == card(RoutesOfSimilarTime[r4])} (r3,r4);

set SameRouteCouples2{r1 in Routes, r2 in RoutesOfSimilarTime[r1]} := setof{r3 in SameRoutes2[r1], r4 in RoutesOfSimilarTime[r3]: r4 in SameRoutes2[r2]} (r3,r4);

set SameRouteCouples3{r1 in Routes, r2 in RoutesOfSimilarTime[r1]} := SameRouteCouples2[r1,r2] diff SameRouteCouples[r1,r2];

set OriginRouteCouples{r1 in Routes, r2 in RoutesOfSimilarTime[r1]} := setof{r3 in OriginRoute[r1], r4 in OriginRoute[r2]: r4 in RoutesOfSimilarTime[r3]} (r3,r4);

set OriginRouteSections{rs in Sections} := setof{r1 in Routes, rs1 in AllRouteSections[r1], r2 in OriginRoute[r1], rs2 in AllRouteSections[r2]: rs1 == rs and ord(rs1) == ord(rs2)} (rs2);

set OriginRouteSectionCouples{(rs1,rs2) in RouteSectionsSharingResource} := setof{rs3 in OriginRouteSections[rs1], rs4 in OriginRouteSections[rs2]} (rs3,rs4);

set OriginRouteSectionCouples2{(rs1,rs2) in RouteSectionsSharingResource} := OriginRouteSectionCouples[rs1,rs2] inter RouteSectionsSharingResource;

# set RoutePathOfRouteSection{rs in Sections} := setof{r in RouteOfRouteSection[rs], rp in RoutePaths[r], rs1 in RouteSections[r,rp]: rs == rs1} (rp);
# set ResourceOccupations2{rs in Sections} := setof{r in RouteOfRouteSection[rs], rp in RoutePathOfRouteSection[rs], res in ResourceOccupations[r,rp,rs]} (res);
# set RouteResourceUsingSections{r in Routes, res in ResourcesOfRoute[r]} := setof{rs in AllRouteSections[r], res1 in ResourceOccupations2[rs]: res == res1} (ord(rs)) ordered;
# set RouteSectionOfOrder{r in Routes} := setof{res in ResourcesOfRoute[r], rs in AllRouteSections[r], rr in RouteResourceUsingSections[r,res]: ord(rs) == rr} (rs);
# set RoutesWithDiscreteResourceUse := setof{r in Routes, res in ResourcesOfRoute[r], rs in RouteResourceUsingSections[r,res]:  (rs != last(RouteResourceUsingSections[r,res]) and next(rs) - rs > 1) or (card(RouteResourceUsingSections[r,res]) > 1 and rs == last(RouteResourceUsingSections[r,res]) and rs - prev(rs) > 1)} (r);
# set RoutesWithDiscreteResourceUse2 := setof{r in Routes, res in ResourcesOfRoute[r], rs in RouteResourceUsingSections[r,res]:  (rs != last(RouteResourceUsingSections[r,res]) and next(rs) - rs > 1) or (card(RouteResourceUsingSections[r,res]) > 1 and rs == last(RouteResourceUsingSections[r,res]) and rs - prev(rs) > 1)} (r,res);
# set FirstRouteSectionOnResource{r in Routes, res in ResourcesOfRoute[r]} := {first(RouteResourceUsingSections[r,res])};

# set DisconnectionPoints{r in Routes, res in ResourcesOfRoute[r]} := setof{rs in RouteResourceUsingSections[r,res]:  (rs != last(RouteResourceUsingSections[r,res]) and next(rs) - rs > 1)} (rs);
# set DisconnectionSections{r in Routes} := setof{res in ResourcesOfRoute[r], rs in DisconnectionPoints[r,res]} (rs);
/*---------------------------------------------------------------------------------------------------------------------------------------
Variables
---------------------------------------------------------------------------------------------------------------------------------------*/
var dentry_time{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;											#section requirement (earliest entry) of service intention si on marked route section rs 
var dexit_time{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;													#section requirement (ear liest entry) of service intention si on marked route section rs 
var yRouteSections{r in Routes, rs in RouteSectionsOfRoute[r]} binary;

var Sentry_time{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;
subject to cSentry_time	{r in Routes, rs in RouteSectionsOfRoute[r]}:
	Sentry_time[r, rs] <= 300000 * (1 - yRouteSections[r, rs]);

var Sexit_time{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;
subject to cSexit_time	{r in Routes, rs in RouteSectionsOfRoute[r]}:
	Sexit_time[r, rs] <= 300000 * (1 - yRouteSections[r, rs]);	


var entry_time{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;
# subject to centry_time1{r in Routes, rs in RouteSectionsOfRoute[r]}:
# 	entry_time[r, rs] <= 300000 * yRouteSections[r, rs];
# subject to centry_time2{r in Routes, rs in RouteSectionsOfRoute[r]}:
# 	entry_time[r, rs] <= dentry_time[r, rs];
# subject to centry_time3{r in Routes, rs in RouteSectionsOfRoute[r]}:
# 	entry_time[r, rs] >= dentry_time[r, rs] - (1-yRouteSections[r, rs]) * 300000;

var exit_time{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;
# subject to cexit_time1{r in Routes, rs in RouteSectionsOfRoute[r]}:
# 	exit_time[r, rs] <= 300000 * yRouteSections[r, rs];
# subject to cexit_time2{r in Routes, rs in RouteSectionsOfRoute[r]}:
# 	exit_time[r, rs] <= dexit_time[r, rs];
# subject to cexit_time3{r in Routes, rs in RouteSectionsOfRoute[r]}:
# 	exit_time[r, rs] >= dexit_time[r, rs] - (1-yRouteSections[r, rs]) * 30000;

/*---------------------------------------------------------------------------------------------------------------------------------------
Constraints
---------------------------------------------------------------------------------------------------------------------------------------*/
subject to cStartAlternative{r in Routes}:
	sum{rs in StartRouteSections[r]} yRouteSections[r, rs] = 1;

subject to cEndAlternative{r in Routes}:
	sum{rs in EndRouteSections[r]} yRouteSections[r, rs] = 1;

subject to cSplitterMixer{r in Routes, am in AlternativeMarkers[r]}:
	sum{rs in RouteSectionsOfRoute[r]: rs in RouteSectionswithMarkeratEntry[r, am] and rs not in StartRouteSections[r]} yRouteSections[r, rs] = 
	sum{rs in RouteSectionsOfRoute[r]: rs in RouteSectionswithMarkeratExit[r, am] and rs not in EndRouteSections[r]} yRouteSections[r, rs];

subject to cNextRouteSection{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp], nrs in NextRouteSections[r, rp, rs]: rs in AllSectionswithNextRouteSections[r]}:
	yRouteSections[r, rs] = yRouteSections[r, nrs];

subject to cSplitterMixerEntry1{r in Routes, am in AlternativeMarkers[r], rs1 in RouteSectionsOfRoute[r], rs2 in RouteSectionsOfRoute[r]
	:rs1 in RouteSectionswithMarkeratExit[r, am] and rs1 not in EndRouteSections[r] and rs2 in RouteSectionswithMarkeratEntry[r, am] and rs2 not in StartRouteSections[r]}:
	Sentry_time[r, rs2] +  entry_time[r, rs2] = exit_time[r, rs1] + Sexit_time[r, rs1];

subject to cNextRouteSectionEntry{r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp], nrs in NextRouteSections[r, rp, rs]: rs in AllSectionswithNextRouteSections[r]}:
	entry_time[r, nrs] = exit_time[r, rs];

subject to c1Rule102{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithEntryEarliest[r]}:
	entry_time[r, rs] >= SectionReqEntryEarliest[m, r] * yRouteSections[r, rs];

subject to c2Rule102{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithExitEarliest[r]}:
	exit_time[r, rs] >= SectionReqExitEarliest[m, r] * yRouteSections[r, rs];

subject to c1Rule103_1{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m]}:
	exit_time[r, rs] - entry_time[r, rs] >= (minRunningTime[r, rs] + minStoppingTime[m, r]) * yRouteSections[r, rs];

subject to c1Rule103_2{r in Routes, rs in RouteSectionsOfRoute[r]}:
	exit_time[r, rs] - entry_time[r, rs] >= minRunningTime[r, rs] * yRouteSections[r, rs];

subject to c1Rule105{(m1, rs1, r1, m2, rs2, r2) in ConnectionConstraint}:
	(1 - yRouteSections[r2, rs2]) * minConnectionTime[m1, r1, r2] + exit_time[r2, rs2] >= entry_time[r1, rs1] + yRouteSections[r1, rs1] * minConnectionTime[m1, r1, r2];

var dummyEntry{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]: rs in RouteSectionsofMarkers[r, m]} >= 0;
var dummyExit{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]: rs in RouteSectionsofMarkers[r, m]} >= 0;
subject to cdummy1{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithEntryLatest[r]}:
	dummyEntry[m, r, rs] >= entry_time[r, rs] - SectionReqEntryLatest[m, r];
subject to cdummy3{m in Markers, r in Routes, rs in RouteSectionsOfRoute[r]:
	rs in RouteSectionsofMarkers[r, m] and rs in RouteSectionsWithExitLatest[r]}:
	dummyExit[m, r, rs] >= exit_time[r, rs] - SectionReqExitLatest[m, r];


var EntryDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} >= 0;
subject to cEntryDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]}:
	EntryDelayPenaltyOfRouteSection[m, r, rp, rs] = SectionReqEntryDelayWeight[m, r] * dummyEntry[m, r, rs];

var EntryDelayPenaltyOfRoutePath{m in Markers, r in Routes, rp in RoutePaths[r]} =
	sum{rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} EntryDelayPenaltyOfRouteSection[m, r, rp, rs];

var EntryDelayPenaltyOfRoute{m in Markers, r in Routes} =
	sum{rp in RoutePaths[r]} EntryDelayPenaltyOfRoutePath[m, r, rp];

var ExitDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} >= 0;
subject to cExitDelayPenaltyOfRouteSection{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]}:
	ExitDelayPenaltyOfRouteSection[m, r, rp, rs] = SectionReqExitDelayWeight[m, r] * dummyExit[m, r, rs];

var ExitDelayPenaltyOfRoutePath{m in Markers, r in Routes, rp in RoutePaths[r]} =
	sum{rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]} ExitDelayPenaltyOfRouteSection[m, r, rp, rs];

var ExitDelayPenaltyOfRoute{m in Markers, r in Routes} =
	sum{rp in RoutePaths[r]} ExitDelayPenaltyOfRoutePath[m, r, rp];

var DelayPenalty = (1/60) * sum{m in Markers, r in Routes, rp in RoutePaths[r], rs in RouteSections[r, rp]: rs in RouteSectionsofMarkers[r, m]}
	(EntryDelayPenaltyOfRouteSection[m, r, rp, rs] + ExitDelayPenaltyOfRouteSection[m, r, rp, rs]);
var PathPenaltyOfRouteSection{r in Routes, rs in RouteSectionsOfRoute[r]} >= 0;
subject to cPathPenaltyOfRouteSection{r in Routes, rs in RouteSectionsOfRoute[r]}:
	PathPenaltyOfRouteSection[r, rs] = Penalty[r, rs]*yRouteSections[r, rs];
var PathPenalty >= 0;
subject to cPathPenalty:
	PathPenalty = sum{r in Routes, rs in RouteSectionsOfRoute[r]}
	PathPenaltyOfRouteSection[r, rs];

var obj_fucntion = DelayPenalty + PathPenalty;

var resourceConflicts = sum{(rs1,rs2) in RouteSectionsSharingResource, r1 in RouteOfRouteSection[rs1], r2 in RouteOfRouteSection[rs2]} (yRouteSections[r1,rs1] + yRouteSections[r2,rs2]);

# var dicon = sum{r in Routes, rs in DisconnectionSections[r]} yRouteSections[r,rs];

/*---------------------------------------------------------------------------------------------------------------------------------------
Objective function
---------------------------------------------------------------------------------------------------------------------------------------*/
minimize objective: obj_fucntion*1000000 + resourceConflicts;# + dicon;
