
Presolve eliminates 93074 constraints and 100712 variables.
Substitution eliminates 837 variables.
Adjusted problem:
9833 variables:
	92 binary variables
	9741 linear variables
13763 constraints, all linear; 27729 nonzeros
	4822 equality constraints
	8941 inequality constraints
1 linear objective; 511 nonzeros.

Gurobi 8.0.0: outlev=1
Optimize a model with 13763 rows, 9833 columns and 27729 nonzeros
Variable types: 9741 continuous, 92 integer (92 binary)
Coefficient statistics:
  Matrix range     [1e-01, 3e+05]
  Objective range  [2e+01, 1e+06]
  Bounds range     [1e+00, 3e+04]
  RHS range        [1e+00, 3e+05]
Presolve removed 13753 rows and 9823 columns
Presolve time: 0.03s
Presolved: 10 rows, 10 columns, 24 nonzeros
Variable types: 9 continuous, 1 integer (1 binary)

Root relaxation: objective 1.253220e+05, 2 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

*    0     0               0    125322.00000 125322.000  0.00%     -    0s

Explored 0 nodes (2 simplex iterations) in 0.03 seconds
Thread count was 8 (of 8 available processors)

Solution count 1: 125322 

Optimal solution found (tolerance 1.00e-04)
Best objective 1.253220000000e+05, best bound 1.253220000000e+05, gap 0.0000%
Optimize a model with 13763 rows, 9833 columns and 27729 nonzeros
Coefficient statistics:
  Matrix range     [1e-01, 3e+05]
  Objective range  [2e+01, 1e+06]
  Bounds range     [1e+00, 3e+04]
  RHS range        [1e+00, 3e+05]
Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    1.2532200e+05   1.781703e+06   0.000000e+00      0s
    4432    1.2532200e+05   0.000000e+00   0.000000e+00      0s

Solved in 4432 iterations and 0.03 seconds
Optimal objective  1.253220000e+05
Gurobi 8.0.0: optimal solution; objective 125322
2 simplex iterations
plus 4432 simplex iterations for intbasis
===========================================
Penalty is:
obj_fucntion = 0

===========================================

Presolve eliminates 192270 constraints and 87636 variables.
Substitution eliminates 976 variables.
Adjusted problem:
121774 variables:
	112248 binary variables
	9526 linear variables
595110 constraints, all linear; 1302468 nonzeros
	474195 equality constraints
	120915 inequality constraints
1 linear objective; 492 nonzeros.

Gurobi 8.0.0: outlev=1
varbranch=2
disconnected=2
Optimize a model with 595110 rows, 121774 columns and 1302468 nonzeros
Variable types: 9526 continuous, 112248 integer (112248 binary)
Coefficient statistics:
  Matrix range     [1e+00, 2e+05]
  Objective range  [2e-02, 2e-02]
  Bounds range     [1e+00, 3e+04]
  RHS range        [1e+00, 4e+04]
Presolve removed 479162 rows and 115810 columns
Presolve time: 2.63s
Presolved: 115948 rows, 5964 columns, 343541 nonzeros
Variable types: 4357 continuous, 1607 integer (1607 binary)
Presolve removed 161 rows and 1 columns
Presolved: 5803 rows, 117554 columns, 344947 nonzeros

Presolve removed 5610 rows and 117072 columns

Root relaxation: objective 0.000000e+00, 18605 iterations, 1.11 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0    0.00000    0    3          -    0.00000      -     -    4s
H    0     0                       0.9833333    0.00000   100%     -   12s
H    0     0                       0.0000000    0.00000  0.00%     -   12s

Explored 1 nodes (19022 simplex iterations) in 12.29 seconds
Thread count was 8 (of 8 available processors)

Solution count 2: 0 0.983333 

Optimal solution found (tolerance 1.00e-04)
Best objective 0.000000000000e+00, best bound 0.000000000000e+00, gap 0.0000%
Optimize a model with 595110 rows, 121774 columns and 1302468 nonzeros
Coefficient statistics:
  Matrix range     [1e+00, 2e+05]
  Objective range  [2e-02, 2e-02]
  Bounds range     [1e+00, 3e+04]
  RHS range        [1e+00, 4e+04]
Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    0.0000000e+00   2.198438e+06   0.000000e+00      0s
   13377    0.0000000e+00   0.000000e+00   0.000000e+00      1s

Solved in 13377 iterations and 1.27 seconds
Optimal objective  0.000000000e+00
Gurobi 8.0.0: optimal solution; objective 0
19022 simplex iterations
1 branch-and-cut nodes
plus 13377 simplex iterations for intbasis
===========================================
Penalty is:
obj_fucntion = 0

===========================================
