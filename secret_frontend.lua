local projectStart = os.time()
local parser = require 'parsers.secretjson2ampldata'
local runfilewriter = require 'parsers.runfilewriter'
local outputparser = require 'parsers.secretamplparser'
local outputwriter = require 'parsers.secretoutputwriter'

-----------------------------------------------------------------------------------------------
-- user input
-----------------------------------------------------------------------------------------------
local whichjson = 2-- choose which json file you want to use from the list below
local ampl_path = '/usr/local/ampl/ampl'
local solver_path = '/usr/local/ampl/gurobi'
local solver_options = 'outlev=1 varbranch=2 disconnected=2' -- --symmetry=2' --'mipgap=0.01 mipdisplay=3' -- presolve=2 --presparsify=1  concurrentmip=8 presparsify=1 -- degenmoves=0 --outlev=1 varbranch=2

-----------------------------------------------------------------------------------------------
-- the list of input json files
-----------------------------------------------------------------------------------------------
local jsonfiles = {
  [0] = 'sample_scenario.json',
  [1] = '01_dummy.json',
  [2] = '02_a_little_less_dummy.json',
  [3] = '03_FWA_0.125.json',
  [4] = '04_V1.02_FWA_without_obstruction.json',
  [5] = '05_V1.02_FWA_with_obstruction.json',
  [6] = '06_V1.20_FWA.json',
  [7] = '07_V1.22_FWA.json',
  [8] = '08_V1.30_FWA.json',
  [9] = '09_ZUE-ZG-CH_0600-1200.json'
}

-----------------------------------------------------------------------------------------------
-- write the .dat file
-----------------------------------------------------------------------------------------------
local data_file = './ampl/problem_'..whichjson..'.dat'
local input_file = 'inputjsons/'..jsonfiles[whichjson] 
local inputdata = parser({input_file = input_file, data_file = data_file})
local amplDatDone = os.time()
print('---------------------------------------------------------------------------------------------')
print(string.format('AMPL dat file is written in: %d seconds', amplDatDone - projectStart))

-----------------------------------------------------------------------------------------------
-- write the .run file
-----------------------------------------------------------------------------------------------
local mustachename = 'parsers/run.mustache'
runfilewriter({
    data_file = data_file,
    solver_path = solver_path,
    solver_name = solver_path:lower():match('cplex') and 'cplex' or solver_path:lower():match('gurobi') and 'gurobi' or solver_path:lower():match('ilogcp') and 'ilogcp',
    solver_options = solver_options,
    problemnumber = whichjson,
  }, mustachename)


-----------------------------------------------------------------------------------------------
-- execute ampl
-----------------------------------------------------------------------------------------------
print(ampl_path..' ./ampl/secret.run > ./ampl/outputfiles/ampllog.txt')
os.execute(ampl_path..' ./ampl/secret.run > ./ampl/outputfiles/ampllog.txt')
local amplDone = os.time()
print('---------------------------------------------------------------------------------------------')
print(string.format('AMPL is done in: %d seconds', amplDone - amplDatDone))

-----------------------------------------------------------------------------------------------
-- parse the ampl results
-----------------------------------------------------------------------------------------------
local amploutput = './ampl/outputfiles/secretresults_p'..whichjson..'.out'
local outputdata = outputparser(amploutput, inputdata.pathofroutesection, inputdata.markerofroutesection, inputdata.allserviceintentions, inputdata.allreqmarkers, whichjson, inputdata.allroutes)
local amplParsed = os.time()
print('---------------------------------------------------------------------------------------------')
print(string.format('AMPL results are parsed in: %d seconds', amplParsed - amplDone))

-----------------------------------------------------------------------------------------------
-- write the output json
-----------------------------------------------------------------------------------------------
local outputjsonname = 'outputjsons/'..jsonfiles[whichjson]:gsub('.json','')..'_results.json'
outputwriter(outputjsonname, inputdata.hash, inputdata.label, outputdata)
local outputWritten = os.time()
print('---------------------------------------------------------------------------------------------')
print(string.format('AMPL results are parsed in: %d seconds', outputWritten - amplParsed))

local projectEnd = os.time()
local projectTime = os.difftime(projectEnd, projectStart)
print('---------------------------------------------------------------------------------------------')
print(string.format('Total project execution time is: %d seconds', projectTime))
print('---------------------------------------------------------------------------------------------')
